#' @export
create_msid_plot<-function(file){
  library(SuperExactTest)
  dat<-read.csv(file,stringsAsFactors = F)
  library(dplyr)
  dat<-filter(dat,!is.na(new_diag))
  
  diagnosis<-unique(dat$new_diag)
  patient_list<<-sapply(diagnosis,function(x) filter(dat,new_diag==x)$patid)
  names(patient_list)<<-toupper(names(patient_list))
  obj<-supertest(patient_list,n=8023)
  
  plot(obj,degree = 2:7,keep.empty.intersections = F,sort.by = 'p-value',color.on = '#008080',Layout = 'landscape',cex=0.7,heatmapColor=colors()[grep("blue",colors())])
  
  
}
