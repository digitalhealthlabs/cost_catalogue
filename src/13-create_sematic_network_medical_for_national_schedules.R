library(dplyr)
library(diagram)
semantic_types<-read.csv("semantic_types.csv")
semantic_types$X<-NULL

national_schedules_outpatient_mapping<-read.csv("/media/Data/DHL/MetaMapToR/national_schedules_to_concepts_map.csv")
national_schedules_outpatient_mapping<-left_join(national_schedules_outpatient_mapping,semantic_types,by=c("utterance_mapping_concept_semantic_types"="accronym"))

xx<-select(national_schedules_outpatient_mapping,text,utterance_mapping_concept_preferred_name,expansion)
xx<-filter(xx,!is.na(expansion))
xx<-distinct(xx)

unique_med_desc<-unique(xx$text)
# length(unique_med_desc)
pdf(file="national_schedules_semantic_networks.pdf")
for(j in 1:length(unique_med_desc)){
  sub_set<-filter(xx,text==unique_med_desc[j])
  concepts<-sub_set$utterance_mapping_concept_preferred_name
  semantic_tpes<-sub_set$expansion
  
  len_concepts<-length(concepts)
  
  par(mar = c(1, 1, 1, 1))
  openplotmat()
  elpos <- coordinates (c(1,1,len_concepts,len_concepts,len_concepts))
  
  d<-list()
  start=3
  for(i in 1:3){
    end=start+len_concepts-1
    d[[i]]<-seq(start,end)
    start=end+1
  }
  a<-matrix(c(1,2),nrow = 1)
  colnames(a)<-c("a","b")
  b<-expand.grid(2,d[[1]])
  colnames(b)<-c("a","b")
  c<-cbind(d[[1]],d[[2]])
  colnames(c)<-c("a","b")
  e<-cbind(d[[2]],d[[3]])
  colnames(e)<-c("a","b")
  f<-rbind(a,b,c,e)
  fromto <- as.matrix(f)
  nr <- nrow(fromto)
  arrpos <- matrix(ncol = 2, nrow = nr)
  for (i in 1:nr)
    arrpos[i, ] <- straightarrow (to = elpos[fromto[i, 2], ],from = elpos[fromto[i, 1], ],
                                  lwd = 2, arr.pos = 0.4, arr.length = 0.4)
  textrect   (elpos[1,], 0.1, 0.05,lab = unique_med_desc[j],     box.col = "lightblue",shadow.col = "darkblue", shadow.size = 0.005, cex = 0.7)
  textrect   (elpos[2,], 0.1, 0.05,lab = "Concepts",     box.col = "lightgreen",shadow.col = "darkblue", shadow.size = 0.005, cex = 0.7)
  
  for( i in 1:len_concepts){
    textrect(elpos[d[[1]][i],], 0.1, 0.05,lab = concepts[i],     box.col = "grey",shadow.col = "darkblue", shadow.size = 0.005, cex = 0.7)
  }
  for(i in 1:len_concepts)
  {
    textrect(elpos[d[[2]][i],], 0.1, 0.05,lab = "Semantic Type",     box.col = "lightgreen",shadow.col = "darkblue", shadow.size = 0.005, cex = 0.7)
  }
  for(i in 1:len_concepts)
  {
    textrect(elpos[d[[3]][i],], 0.1, 0.05,lab =  semantic_tpes[i],     box.col = "grey",shadow.col = "darkblue", shadow.size = 0.005, cex = 0.7)
  }
  
}
dev.off()


