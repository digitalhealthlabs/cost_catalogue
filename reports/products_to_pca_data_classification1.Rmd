---
title: "Products to PCA match algorithm"
author: "Vineet Berlia"
date: "13 May 2016"
output: pdf_document
---

## Important points/ideas/observations about patterns and structure of data in the PCA data and CPRD products dictionary :-

1. There are only two source of information in the PCA data
  + BNF chemical name
  + Drug Name: Many of the drug names are not the actual drug names, they are just truncated forms of the chemical names for eg. Magnesium Oxide has drug name Mag Ox_Tab 400mg which creates problem in classification.
2. In the product names in CPRD databse mostly every thing inside the braces are names of the pharma companies.
So, it can be used at the later stage as the cost of the same drugs by different manufacturers will vary.
3. There are many categories of products like tablets,capsules, eye drops etc. So, I think instead of developing logic to classify all at once it will be good idea to separate them and classify them separately.
There are around 10000 tablets, 2000 capsules, 2000 oral soln and there structures are quiet complex and different.
  

## Things which I want to do which can aid in classification:-

1. Find the relationship between drug name and drug-substance and do something with metamap to find the lexical variations and semantic types.

2. In classification of drugs metamap will not help much as metamap does not have very deep understanding of the 
chemical names. Therefore, I need to study RxNorm which understands drug names and figure out if it can aid in classification.

3. Saying NLP is not able to classify the products and saying that there is not enough data for classification are two different things: So, I want to write separate algorithm to find the ones that can not be classified due to insufficient data and hence remove them from to be classified candidate which will also increase the algorithm efficiency.

4. Here in this case of drug classification five things are important
  + Exact drug name(without the strength,formulation etc)
  + strength
  + formulation(tab,cap etc)
  + chemical substance
  + manufacturing pharma company
Need to figure out which is absoultely necessary and which not. Also, there can be various possible relations among them of (and/or).

4. Form the various groups from the conditions generated from above and draw the wordnetwork and word clouds to find the hidden patterns in the data.

The flowchart below shows the algorithm developed so far to classify the tablet products:-
